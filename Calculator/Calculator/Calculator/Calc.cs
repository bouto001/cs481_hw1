﻿using System.Data;

namespace Calculator
{
    public static class Calc
    {
        public static string ComputeCalculation(string input)
        {
            try
            {
                var res = new DataTable()
                    .Compute(input.Replace("x", " * "), null)
                    .ToString();

                return res;
            }
            catch
            {
                return "ERROR";
            }
        }
    }
}
